package com.sterling.microservices.myservice.dtos;

import lombok.Getter;
import org.springframework.hateoas.ResourceSupport;

import java.time.LocalDate;

/**
 * @author cbongiorno on 3/19/18.
 */
public class Report extends ResourceSupport {

    @Getter
    private LocalDate fileDate;

    @Getter
    private String type;
}
