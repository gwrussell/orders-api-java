#!groovy


def image

node('linux' && 'docker') {

    stage('SCM') {
        checkout scm
    }

    stage('Package') {
        sh "mvn package -DskipTests"
    }

    stage('Test') {
        sh "mvn verify"
    }

    stage('Publish') {
        withCredentials([usernamePassword(credentialsId: "${env.JFROG_CREDENTIALS}",
                passwordVariable: 'REPO_PASS',
                usernameVariable: 'REPO_USER')]) {

            mvnCmd = env.BRANCH_NAME == 'master' ? "release:prepare release:perform" : "deploy"
            sh "mvn ${mvnCmd} -Ddocker.username=${REPO_USER} -Ddocker.password=${REPO_PASS}"
        }

    }
    stage('deploy') {
        withAWS(profile: env.AWS_PROFILE, region: env.REGION) {
            cfnUpdate(
                    stack: "${artifactId}-${env.ENVIRONMENT_NAME}",
                    file: 'cft.yml',
                    params: [
                            'Environment=' + env.ENVIRONMENT_NAME,
                            'EnvironmentContext=' + env.VPC,
                            'Image=' + image,
                            'AppName=' + artifactId,
                            'Profiles=' + env.BOOT_PROFILES,
                    ],
                    keepParams: ['EnvironmentName', 'Vpc', 'AppName']
            )
        }
    }
}