package com.sterlingts.microservices.ordersservice;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.sterlingts.microservices.ordersservice.generators.IdGenerator;
import com.sterlingts.microservices.ordersservice.generators.ScatterGatherIdGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.validation.Validator;

import javax.sql.DataSource;

@SpringBootApplication
@EnableAutoConfiguration
public class OrdersApp {

    public static void main(String[] args) {
        SpringApplication.run(OrdersApp.class, args);
    }

    @Configuration
    @RequiredArgsConstructor
    public static class ValidationConfiguration extends RepositoryRestConfigurerAdapter {

        private final Validator jsr303Validator;

        @Override
        public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
            //bean validation always before save and create
            validatingListener.addValidator("beforeCreate", jsr303Validator);
            validatingListener.addValidator("beforeSave", jsr303Validator);

        }


    }

    @Bean
    public AmazonDynamoDB dynamo( @Value("${aws.}") String region) {
        return  AmazonDynamoDBClientBuilder.standard()
                .withRegion(region)
                .build();
    }

    @Bean
    public IdGenerator idGen() {
        return new ScatterGatherIdGenerator();
    }

    @Bean
    @Profile("default")
    public DataSource dataSource() {
        // no need shutdown, EmbeddedDatabaseFactoryBean will take care of this
        return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).build();
    }


}
