package com.sterlingts.microservices.ordersservice.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.CompositeHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.OrderedHealthAggregator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class HealthController {


  private final Map<String,HealthIndicator> indicators;

  @Autowired
  public HealthController(Map<String, HealthIndicator> indicators) {
    this.indicators = indicators;
  }



  @GetMapping("/health")
  public Health getHealth(@RequestParam("deep") Boolean deep){
    if(deep != null && deep)
      return new CompositeHealthIndicator(new OrderedHealthAggregator(),indicators).health();

    return Health.up().build();
  }


}
