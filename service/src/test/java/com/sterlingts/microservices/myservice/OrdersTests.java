package com.sterlingts.microservices.myservice;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sterlingts.microservices.ordersservice.domain.resources.Order;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= WebEnvironment.RANDOM_PORT)
public class OrdersTests {

	@Autowired
	private TestRestTemplate restTemplate;
	private final ParameterizedTypeReference<Resource<Order>> type = new ParameterizedTypeReference<Resource<Order>>() {
	};

	/*@Test
	public void fetchAHateosObject() {
		Order expected = new Order("12310","1232323233");
		// a java legacy
		ResponseEntity<Resource<Order>> actual = restTemplate.exchange("/orders/1", HttpMethod.GET, null, type);

		assertThat(actual.getBody().getContent().getReferenceValue()).isEqualTo(expected.getReferenceValue());

	}*/

	@Test
	public void testInvalidPayload() {
		Order input = new Order("12310!!", "fakesalesforce");
		// a java legacy

		ResponseEntity<ValidationFailure> reponse = restTemplate
				.postForEntity("/orders", input, ValidationFailure.class);

		assertThat(reponse.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

		ValidationFailure expected = new ValidationFailure(new ArrayList<>(
				singletonList(new ValidationError("Order", "referenceValue", input.getReferenceValue()))));

		assertThat(reponse.getBody()).isEqualTo(expected);


	}

	@AllArgsConstructor
	@NoArgsConstructor
	@ToString
	@EqualsAndHashCode
	public static class ValidationFailure {

		@JsonProperty("errors")
		List<ValidationError> errors;
	}

	@AllArgsConstructor
	@NoArgsConstructor
	@ToString
	@EqualsAndHashCode
	public static class ValidationError {

		public String entity;
		public String property;
		public Object invalidValue;
		// don't care what the message is
//		public String message;
	}



}
