package com.sterlingts.microservices.myservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sterling.microservices.myservice.dtos.Report;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.client.Traverson;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.Arrays;


/**
 * @author cbongiorno on 3/19/18.
 */
public class MyClientTest {

    @Test()
    @Disabled
    void foo() {
        URI baseUri = URI.create("http://localhost:8080");

        ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Arrays.asList(MediaTypes.HAL_JSON_UTF8, MediaTypes.HAL_JSON));
        converter.setObjectMapper(mapper);

        RestTemplate template = new RestTemplate();
        template.setMessageConverters(
                Arrays.asList(new StringHttpMessageConverter(Charset.forName("UTF-8")), converter));

        Traverson traverson = new Traverson(baseUri, MediaTypes.HAL_JSON_UTF8, MediaTypes.HAL_JSON);
        traverson.setRestOperations(template);

        Resources<Report> date =
                traverson.follow("reports").toObject(new ParameterizedTypeReference<Resources<Report>>() {
                });
        System.out.println();
    }


}
