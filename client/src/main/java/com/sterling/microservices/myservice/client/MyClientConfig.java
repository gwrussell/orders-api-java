package com.sterling.microservices.myservice.client;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author cbongiorno on 3/19/18.
 */
@Configuration
public class MyClientConfig {

    @Bean
    MyClient myClient() {
        return new MyClient();
    }
}
