Kotlin scaffolding
--


This repo demonstrates the following things:

1. Fully functioning `spring-data-rest` integration with a mysql database. Use ` --spring.profiles.active=mysql` to 
enable. Make sure you have a mysql container running. Because the connection string needs a ready schema and 
out-of-the-box only a few exist, then it writes to `mysql` schema by default. You may use the tables created to extract 
ddl for your entities

```

mysql> describe mysql.form;
+-------------+------------+------+-----+---------+----------------+
| Field       | Type       | Null | Key | Default | Extra          |
+-------------+------------+------+-----+---------+----------------+
| id          | bigint(20) | NO   | PRI | NULL    | auto_increment |
| file_date   | date       | YES  |     | NULL    |                |
| type        | int(11)    | YES  |     | NULL    |                |
| employee_id | bigint(20) | YES  | MUL | NULL    |                |
+-------------+------------+------+-----+---------+----------------+
4 rows in set (0.00 sec)

mysql> describe mysql.employee;
+-----------+--------------+------+-----+---------+----------------+
| Field     | Type         | Null | Key | Default | Extra          |
+-----------+--------------+------+-----+---------+----------------+
| id        | bigint(20)   | NO   | PRI | NULL    | auto_increment |
| email     | varchar(255) | YES  |     | NULL    |                |
| hire_date | date         | YES  |     | NULL    |                |
| name      | varchar(255) | YES  |     | NULL    |                |
+-----------+--------------+------+-----+---------+----------------+
4 rows in set (0.01 sec)

mysql> describe mysql.mysql> describe mysql.report;
+-------------+------------+------+-----+---------+----------------+
| Field       | Type       | Null | Key | Default | Extra          |
+-------------+------------+------+-----+---------+----------------+
| id          | bigint(20) | NO   | PRI | NULL    | auto_increment |
| file_date   | date       | YES  |     | NULL    |                |
| type        | int(11)    | YES  |     | NULL    |                |
| employee_id | bigint(20) | YES  | MUL | NULL    |                |
+-------------+------------+------+-----+---------+----------------+
4 rows in set (0.00 sec)
```

Running the below command will give you a DDL dump that you can develop further

```bash
mysqldump -d -u root -pmy-secret-pw -h 127.0.0.1 mysql form employee report 
```

2. JSR303 validation is enabled. Your entities will be validated anytime they are created or updated and produce a `400`
response along with details of the failures

3. There is a resource: `/health?deep={false|true}` that produces a health check based on the `HealthIndicator`s in 
your classpath


[![My Stack Overflow Profile](https://stackexchange.com/users/flair/673865.png)](http://stackexchange.com/users/673865)

[![Book session on Codementor](https://cdn.codementor.io/badges/book_session_github.svg)](https://www.codementor.io/chb0codementor?utm_source=github&utm_medium=button&utm_term=chb0codementor&utm_campaign=github)

